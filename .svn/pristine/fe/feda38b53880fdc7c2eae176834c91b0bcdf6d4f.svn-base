<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vue-router@3.0.0/dist/vue-router.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: inherit;
        }

        html {
            box-sizing: border-box;
            height: 100vh;
            width: 100vw;
        }

        #app {
            height: 100%;
            width: 100%;
            background-color: transparent;
        }

        .container {
            padding: 40px;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .header {
            display: flex;
            justify-content: center;
        }

        .viewing_action {
            display: flex;
            flex-direction: row;
            column-gap: 5px;
            background-color: transparent;
            justify-content: space-between;
        }

        .body {
            display: flex;
            flex: 1;
            background-color: transparent;
            padding: 20px 0px;
        }

        .main_screen,
        .create_edit_screen {
            padding: 20px;
        }

        .task_item_container {
            display: flex;
            flex-direction: row;
            column-gap: 10px;
        }

        .bi::before {
            font-size: 16px;
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="container">
            <h2 class="header">Todo App</h2>
            <div class="main_screen" v-if="context == contextConst.isViewing">
                <div class="viewing_action">
                    <button class="btn btn-primary" v-on:click="navigateToCreate">Create task</button>
                    <div>
                        <!-- <button class="btn btn-outline-secondary" v-on:click="restore">Restore</button>
                        <button class="btn btn-secondary" v-on:click="backup">Backup</button> -->
                    </div>
                </div>
                <div class="body">
                    <ol v-if="tasks.length > 0">
                        <task
                            v-for="task in tasks"
                            :key="task.id"
                            :task="task"
                            v-on:edit-task="navigateToEditing($event)"
                            v-on:delete-task="deleteTask($event)"
                        ></task>
                    </ol>
                    <div v-else>
                        <p>List task is empty</p>
                    </div>
                </div>

            </div>
            <div class="create_edit_screen"
                v-if="context == contextConst.isCreating || context == contextConst.isEditing">
                <form v-on:submit.prevent="submit">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Task name:</label>
                        <input type="text" class="form-control" v-model="name" id="exampleFormControlInput1"
                            placeholder="Enter task name">
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-primary form-control" type="submit">
                            {{contextConst.isCreating == context ? 'create' : 'edit'}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <script>
        // context state
        const baseUrl = 'https://638ea22a9cbdb0dbe3112e9e.mockapi.io';
        const contextConst = {
            isCreating: 1,
            isEditing: 2,
            isViewing: 3,
        };
        class Task {
            constructor(id, name) {
                this.id = id;
                this.name = name;
            }
        }

        Vue.use(VueRouter);

        const vue = new Vue({
            el: '#app',
            data: {
                // 
                id: null,
                name: null,
                //
                tasks: [],
                context: contextConst.isViewing,
                contextConst: contextConst,
            },
            // lifecycle hooks 
            created: async function () {
                await this.fetch();
            },
            methods: {
                // context methods
                navigateToCreate: function () {
                    this.context = this.contextConst.isCreating;
                    this.id = null;
                    this.name = '';
                },
                navigateToViewing: function () {
                    this.context = this.contextConst.isViewing;
                },
                navigateToEditing: async function (taskId) {
                    this.context = this.contextConst.isEditing;
                    let response = await axios.get(`${baseUrl}/tasks/${taskId}`);
                    if (response.status >= 200 && response.status < 300) {
                        this.id = response.data.id;
                        this.name = response.data.name;
                    }
                },
                //
                submit: function () {
                    if (this.context == this.contextConst.isCreating) {
                        this.create();
                    } else {
                        this.edit();
                    }
                },
                fetch: async function () {
                    let response = await axios.get(`${baseUrl}/tasks`);
                    if (response.status >= 200 && response.status < 300 && Array.isArray(response.data) && response.data.length > 0) {
                        this.tasks = response.data;
                        this.tasks.sort((a, b) => a - b);
                    }
                },
                edit: async function () {
                    if (this.name && this.name.length > 0) {
                        let response = await axios.put(`${baseUrl}/tasks/${this.id}`, {
                            name: this.name,
                        });
                        if (response.status >= 200 && response.status < 300) {
                            var index = this.tasks.findIndex(e => e.id == this.id);
                            this.tasks.splice(index, 1, new Task(response.data.id, response.data.name));
                            this.navigateToViewing();
                        }
                    }
                },
                create: async function () {
                    if (this.name && this.name.length > 0) {
                        let response = await axios.post(`${baseUrl}/tasks`, {
                            name: this.name
                        });
                        if (response.status >= 200 && response.status < 300) {
                            this.tasks.push(new Task(response.data.id, response.data.name));
                            this.navigateToViewing();
                        }
                    }
                },
                deleteTask: async function (taskId) {
                    if (confirm("Are you sure to remove this task?")) {
                        let response = await axios.delete(`${baseUrl}/tasks/${taskId}`);
                        if (response.status >= 200 && response.status < 300) {
                            var index = this.tasks.findIndex(e => e.id == taskId);
                            this.tasks.splice(index, 1);
                        }
                    }
                },
                backup: function () {
                    localStorage.setItem('tasks', JSON.stringify(this.tasks));
                },
                restore: function () {
                    var backupData = JSON.parse(localStorage.getItem('tasks'));
                    if (backupData != null && backupData != undefined && backupData.length != 0) {
                        for (var data of backupData) {
                            if (this.tasks.findIndex(e => e.id == data.id) == -1) {
                                this.tasks.push(data);
                            }
                        }
                        this.tasks.sort((a, b) => a.id - b.id);
                    }
                },
            },
        });

        Vue.component('task', {
            props: ['task'],
            template: /* html */`
                <li class="mb-3">
                    <div class="task_item_container">
                        <span>{{task.name}}</span>
                        <button class="btn btn-sm btn-outline-warning"
                            v-on:click="$emit('edit-task', task.id)"><i class="bi bi-pencil"></i></button>
                        <button class="btn btn-sm btn-outline-danger" v-on:click="$emit('delete-task', task.id)"><i class="bi bi-trash3"></i></button>
                    </div>
                </li>
            `,
        });
    </script>
</body>

</html>