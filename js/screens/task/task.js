const Tasks = {
    data: function () {
        return {
            tasks: [],
            users: [],
            status: taskStatus,
        };
    },
    // lifecycle hooks 
    created: async function () {
        await this.fetch();
    },
    methods: {
        fetch: async function () {
            let response = await axios.get(`${baseUrl}/tasks`);
            let usersResponse = await axios.get(`${baseUrl}/users`);
            if (response.status >= 200 && response.status < 300 && Array.isArray(response.data) && response.data.length > 0) {
                this.tasks = response.data;
                this.tasks.sort((a, b) => a - b);
            }
            if (usersResponse.status >= 200 && usersResponse.status < 300 && Array.isArray(usersResponse.data) && usersResponse.data.length > 0) {
                this.users = usersResponse.data;
                this.users.sort((a, b) => a - b);
            }
        },
        removeTask: async function (taskId) {
            if (confirm("Are you sure to remove this task?")) {
                let response = await axios.delete(`${baseUrl}/tasks/${taskId}`);
                if (response.status >= 200 && response.status < 300) {
                    var index = this.tasks.findIndex(e => e.id == taskId);
                    this.tasks.splice(index, 1);
                }
            }
        },
        getUserNameById: function (userId) {
            // console.log(userId);
            if (userId == null || userId == undefined) return 'Empty';
            let user = this.users.find(user => user.idUser == userId);
            if (user == null || user == undefined) return 'Empty';
            return user.name;
        },
        editTask: function (idTask) {
            this.$router.push({ path: `/tasks/edit/${idTask}` });
        }
    },
    template: /* html */`
        <div>
            <div class="viewing_action">
                <router-link class="btn btn-primary" to="/tasks/create">Create task</router-link>
                <div>
                </div>
            </div>
            <div class="body">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Task name</th>
                          <th scope="col">Assignee</th>
                          <th scope="col">Status</th>
                          <th scope="col">Actions</th>
                        </tr>
                    </thead>
                      <tbody>
                        <template v-if="tasks.length > 0">
                            <tr v-for="(task, index) in tasks" :key="task.idTask">
                                <th scope="row">{{index + 1}}</th>
                                <td>{{task.name}}</td>
                                <td>{{getUserNameById(task.idAssignee)}}</td>
                                <td>{{status[task.status].name}}</td>
                                <!-- <td>kk</td> -->
                                <td>
                                    <button class="btn btn-sm btn-outline-warning" @click="editTask(task.idTask)"><i class="bi bi-pencil"></i></button>
                                    <button class="btn btn-sm btn-outline-danger" @click="removeTask(task.idTask)"><i class="bi bi-trash3"></i></button>
                                </td>
                            </tr>
                        </template>
                        <tr v-else>
                            <td colspan="5" style="text-align: center;">List task is empty</td>
                        </tr>
                    </tbody>
                </table>
                <!-- <ol v-if="tasks.length > 0"> -->
                    <!-- <task v-for="task in tasks" :key="task.id" :task="task"
                        v-on:edit-task="navigateToEditing($event)" v-on:delete-task="deleteTask($event)"></task> -->
                <!-- </ol>
                <div v-else>
                    <p>List task is empty</p>
                </div> -->
            </div>
        </div>
    `,
};