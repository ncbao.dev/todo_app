const TaskModify = {
    data: function () {
        return {
            taskStatus,
            id: null,
            name: '',
            status: taskStatus[0].value,
            user: null,
            users: [],
        };
    },
    created: async function () {
        let response = await axios.get(`${baseUrl}/users`);
        if (response.status >= 200 && response.status < 300) {
            this.users = response.data;
        }
        if (this.$route.params.id) {
            let task = await axios.get(`${baseUrl}/tasks/${this.$route.params.id}`);
            this.name = task.data.name;
            this.status = task.data.status;
            this.user = task.data.idAssignee ? this.users.find(user => user.idUser == task.data.idAssignee) : null;
            this.id = task.data.id;
        }
    },
    methods: {
        handleSelectStatus: function (e) {
            this.status = e.target.value;
        },
        submit: async function () {
            if (this.$route.params.id) {
                let response = await axios.put(`${baseUrl}/tasks/${this.$route.params.id}`, {
                    name: this.name,
                    status: this.status,
                    idAssignee: this.user && this.user.idUser ? this.user.idUser : null,
                });
                if (response.status >= 200 && response.status < 300) {
                    this.$router.back();
                }
            } else {
                let response = await axios.post(`${baseUrl}/tasks`, {
                    name: this.name,
                    status: this.status,
                    idAssignee: this.user && this.user.idUser ? this.user.idUser : null,
                });
                if (response.status >= 200 && response.status < 300) {
                    this.$router.back();
                }
            }
        },
    },
    template: /* html */`
        <form v-on:submit.prevent="submit">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Task name:</label>
                <input type="text" class="form-control" v-model="name" id="exampleFormControlInput1"
                    placeholder="Enter task name" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Status:</label>
                <select class="form-select" @change="handleSelectStatus">
                    <option v-for="stats in taskStatus" :value="stats.value" :selected="stats.value == status" :key="stats.value">{{stats.name}}</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Assignee:</label>
                    <!-- <v-select :options="users" label="name" v-model="idAssignee" :reduce="user => user.idUser" placeholder="Choose a user to assign"></v-select> -->
                <v-select :options="users" label="name" v-model="user" placeholder="Choose a user to assign"></v-select>
            </div>
            <div class="mb-3">
                <button class="btn btn-primary form-control" type="submit">
                    {{this.$route.params.id ? 'Update' : 'Create'}}
                </button>
            </div>
        </form>
    `,
};