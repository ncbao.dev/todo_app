const Account = {
    data: function () {
        return {
            users: [],
        };
    },
    // lifecycle hooks 
    created: async function () {
        await this.fetch();
    },
    methods: {
        fetch: async function () {
            let response = await axios.get(`${baseUrl}/users`);
            if (response.status >= 200 && response.status < 300 && Array.isArray(response.data) && response.data.length > 0) {
                this.users = response.data;
                this.users.sort((a, b) => a - b);
            }
        },
        removeAccount: async function (userId) {
            if (confirm("Are you sure to remove this user?")) {
                let response = await axios.delete(`${baseUrl}/users/${userId}`);
                if (response.status >= 200 && response.status < 300) {
                    var index = this.users.findIndex(e => e.idUser == userId);
                    this.users.splice(index, 1);
                }
            }
        },
        editAccount: function (userId) {
            this.$router.push({ path: `/accounts/edit/${userId}` });
        }
    },
    template: /* html */`
        <div>
            <div class="viewing_action">
                <router-link class="btn btn-primary" to="/accounts/create">Create user</router-link>
                <div>
                </div>
            </div>
            <div class="body">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">User name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Password</th>
                          <th scope="col">Actions</th>
                        </tr>
                    </thead>
                      <tbody>
                        <template v-if="users.length > 0">
                            <tr v-for="(user, index) in users" :key="user.idUser">
                                <th scope="row">{{index + 1}}</th>
                                <td>{{user.name}}</td>
                                <td>{{user.email}}</td>
                                <td>{{user.password}}</td>
                                <!-- <td>kk</td> -->
                                <td>
                                    <button class="btn btn-sm btn-outline-warning" @click="editAccount(user.idUser)"><i class="bi bi-pencil"></i></button>
                                    <button class="btn btn-sm btn-outline-danger" @click="removeAccount(user.idUser)"><i class="bi bi-trash3"></i></button>
                                </td>
                            </tr>
                        </template>
                        <tr v-else>
                            <td colspan="5" style="text-align: center;">List user is empty</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `,
};