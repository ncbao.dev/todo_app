const AccountModify = {
    data: function () {
        return {
            name: '',
            email: '',
            password: '',
        };
    },
    created: async function () {
        if (this.$route.params.id) {
            let user = await axios.get(`${baseUrl}/users/${this.$route.params.id}`);
            this.name = user.data.name;
            this.email = user.data.email;
            this.password = user.data.password;
        }
    },
    methods: {
        submit: async function () {
            if (this.$route.params.id) {
                let response = await axios.put(`${baseUrl}/users/${this.$route.params.id}`, {
                    name: this.name,
                    email: this.email,
                    password: this.password,
                });
                if (response.status >= 200 && response.status < 300) {
                    this.$router.back();
                }
            } else {
                let response = await axios.post(`${baseUrl}/users`, {
                    name: this.name,
                    email: this.email,
                    password: this.password,
                });
                if (response.status >= 200 && response.status < 300) {
                    this.$router.back();
                }
            }
        },
    },
    template: /* html */`
        <form v-on:submit.prevent="submit" >
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">User name:</label>
                <input type="text" class="form-control" v-model="name" id="exampleFormControlInput1"
                    placeholder="Enter user name" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput2" class="form-label">Email:</label>
                <input type="email" class="form-control" v-model="email" id="exampleFormControlInput2"
                    placeholder="Enter email address" required>
            </div>
            <div class="mb-3">
                <label for="psw" class="form-label">Password:</label>
                <input type="password" id="psw" name="psw" class="form-control" v-model="password"
                    placeholder="Enter password" pattern="[A-Z]+[a-z]+[0-9]+" title="Must contain at least one number and one uppercase and lowercase letter" required>                        
            </div>
            <div class="mb-3">
                <button class="btn btn-primary form-control" type="submit">
                    {{this.$route.params.id ? 'Update' : 'Create'}}
                </button>
            </div>
        </form>
    `,
};