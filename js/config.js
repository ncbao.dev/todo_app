const baseUrl = 'https://638ea22a9cbdb0dbe3112e9e.mockapi.io';

const taskStatus = [
    {
        name: "Open",
        value: 0,
    },
    {
        name: "Progress",
        value: 1,
    },
    {
        name: "Done",
        value: 2,
    },
];
class Task {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}