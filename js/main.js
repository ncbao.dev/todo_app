Vue.component('v-select', VueSelect.VueSelect);

const router = new VueRouter({
    routes: [
        { path: '/', redirect: '/tasks' },
        { path: '/tasks', component: Tasks },
        { path: '/tasks/create', component: TaskModify },
        { path: '/tasks/edit/:id', component: TaskModify },
        { path: '/accounts', component: Account },
        { path: '/accounts/create', component: AccountModify },
        { path: '/accounts/edit/:id', component: AccountModify },
    ],
    linkActiveClass: 'active',
});

const vue = new Vue({
    router
}).$mount('#app');